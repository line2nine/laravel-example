<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::get('/', [UserController::class, 'index'])->name('index');

    Route::get('create', [UserController::class, 'create'])->name('user.create');
    Route::post('create', [UserController::class, 'store']);

    Route::get('edit/{id}', [UserController::class, 'edit'])->name('user.edit');
    Route::post('edit/{id}', [UserController::class, 'update']);

    Route::get('delete/{id}', [UserController::class, 'destroy'])->name('user.delete');

    Route::get('search', [UserController::class, 'search'])->name('user.search');
});

Route::get('register', [AuthController::class, 'register'])->name('register');
Route::post('register', [AuthController::class, 'postRegister']);

Route::get('login', [AuthController::class, 'login'])->name('login');
Route::post('login', [AuthController::class, 'postLogin']);

Route::get('logout', [AuthController::class, 'logout'])->name('logout');

