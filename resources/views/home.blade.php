@extends('master')
@section('content')
    <h2 class="text-center mt-5 mb-5">User Management</h2>
    <div class="mb-2">
        <a href="{{route('user.create')}}" class="btn btn-success">Create</a>
        <div style="float: right">
            <form class="form-inline my-2 my-lg-0" action="{{route('user.search')}}" method="GET">
                @csrf
                <input class="form-control mr-sm-2" name="keyword" type="search" placeholder="Search..." aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($users))
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>
                        <a class="btn btn-warning" href="{{route('user.edit', $user->id)}}">Edit</a> |
                        <a class="btn btn-danger" onclick="return confirm('Are You Sure!?')"
                           href="{{route('user.delete', $user->id)}}">Delete</a>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
@endsection
