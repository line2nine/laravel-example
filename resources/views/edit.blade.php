@extends('master')
@section('content')
    <h2 class="text-center mt-5 mb-5">Edit User</h2>
    <form action="{{route('user.edit', $user->id)}}" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" name="name" @if(!empty($user)) value="{{$user->name}}" @endif class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" name="email" @if(!empty($user)) value="{{$user->email}}" @endif class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        </div>
        <a href="{{route('index')}}" class="btn btn-secondary">Back</a>
        <button type="submit" class="btn btn-success">Update</button>
    </form>
@endsection
