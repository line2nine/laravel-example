<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Eloquent\UserEloquentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserEloquentRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $users = $this->userRepository->all();
        return view('home', [
            'users' => $users
        ]);
    }

    public function create()
    {
       return view('create');
    }

    public function store(Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $password = $request->get('password');

        $user = [
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password)
        ];

        $this->userRepository->create($user);
        return redirect()->route('index');
    }

    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        return view('edit', [
            'user' => $user
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        $userId = $user->id;

        $name = $request->get('name');
        $email = $request->get('email');

        $data = [
            'name' => $name,
            'email' => $email
        ];

        $this->userRepository->update($data, $userId);
        return redirect()->route('index');
    }

    public function destroy($id)
    {
        $user = $this->userRepository->find($id);
        $userId = $user->id;

        $this->userRepository->delete($userId);
        return redirect()->route('index');
    }

    public function search(Request $request)
    {
        $keyword = $request->get('keyword');

//        1. Use function in Repo
        $users = $this->userRepository->searchByName($keyword);
//        2. Use custom query
//        $users = $this->userRepository->customQuery()->where('name', 'LIKE', '%' . $keyword . '%')->get();

        return view('home', [
            'users' => $users
        ]);
    }
}
